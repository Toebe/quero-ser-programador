# **Porque eu acho que seria um bom programador?**

## Gosto de:
    * Raciocínio lógico;
    * Resolver problemas.
## Lido bem com:
    * Desafios;
    * Mudanças.
## Tenho interesse em:
    * Seguir na área;
    * Se tornar um bom profissional da área.

*Acho que por isso seria um bom programador.* :)